package com.aniket.tutorial1;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class ProducerDemo {
    public static void main(String[] args) {

        //Set Producer properties
        Properties properties = new Properties();
        //Create the producer
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String,String> kafkaProducer = new KafkaProducer(properties);

        ProducerRecord<String,String> record = new ProducerRecord<String, String>("first_topic","Hello world");


        //send data -This is async Code

        kafkaProducer.send(record);
//Wait until Sending completed then flushing
        kafkaProducer.flush();
        //Closing connection
        kafkaProducer.close();
    }
}
