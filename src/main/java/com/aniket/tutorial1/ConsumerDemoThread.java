package com.aniket.tutorial1;

import com.google.gson.Gson;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerDemoThread {
    public static final Logger logger = LoggerFactory.getLogger(ConsumerDemoThread.class.getName());

    public static void main(String[] args) throws InterruptedException {

        //Set Consumer properties
        Properties properties = new Properties();
        //Create the Consumer
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "my_group");
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");

CountDownLatch countDownLatch = new CountDownLatch(1);
Runnable myConsumerThread = new ConsumerThread(countDownLatch,"first_topic",properties);

Thread myRunnableThread = new Thread(myConsumerThread);
myRunnableThread.start();

//Shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(()->{
            logger.info("Detected program shutdown - let's shutdown consumer also");
            ((ConsumerThread) myConsumerThread).shutDown();
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("--Application is existed-----");
        }));

 logger.info("awating for consumer shutdown----");
countDownLatch.await();



logger.info("--main method gone away ----");

    }


}

class ConsumerThread implements  Runnable
{
    public static final Logger logger = LoggerFactory.getLogger(ConsumerDemoThread.class.getName());

    private CountDownLatch countDownLatch;
    private   KafkaConsumer<String,String> kafkaConsumer;
    private String topic;
    private  Properties properties;

    public ConsumerThread(CountDownLatch countDownLatch,String topic
            ,Properties properties) {
        this.countDownLatch = countDownLatch;
        this.topic=topic;
        this.properties=properties;
        kafkaConsumer = new KafkaConsumer<String, String>(properties);
        kafkaConsumer.subscribe(Arrays.asList(topic));
    }


    public void shutDown()
    {
        //To interrupt the polling process and will throw wakeupexception
        logger.info("----Let's initate consumer shutdown----");
        kafkaConsumer.wakeup();
    }

    @Override
    public void run() {
        try {
            while (true) {
                ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord record : records) {
                    logger.info("Message Info : " + new Gson().toJson(record));
                }
            }
        }catch (WakeupException e)
        {
logger.info("We have received info to shutdown");
        }
        finally {
            kafkaConsumer.close();
            countDownLatch.countDown();
        }
    }
}
