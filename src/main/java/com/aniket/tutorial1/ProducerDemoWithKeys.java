package com.aniket.tutorial1;

import com.google.gson.Gson;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerDemoWithKeys {

    public static final Logger logger = LoggerFactory.getLogger(ProducerDemoWithKeys.class.getName());
    public static void main(String[] args) throws InterruptedException {

        //Set Producer properties
        Properties properties = new Properties();
        //Create the producer
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String,String> kafkaProducer = new KafkaProducer(properties);
        Thread.sleep(3000);
        for (int i = 0; i <300 ; i++) {

            String first_topic = "first_topic";

            String msg = "Hello world_" + i;
           // final String key = "key_" + (i % 2);
           // ProducerRecord<String,String> record = new ProducerRecord<String, String>(first_topic, key, msg);


            ProducerRecord<String,String> record = new ProducerRecord<String, String>(first_topic, msg);


            //send data

        kafkaProducer.send(record, new Callback() {
            public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                //Execute every time record being sent
                if(e==null)
                {
                   // logger.info(key+"-------partition_key_mapping-----"+recordMetadata.partition());
                    logger.info("--------METADATA INFO-----"+new Gson().toJson(recordMetadata));
                }
                else
                {
                    logger.error("--------ERROR INFO-----"+e.getMessage());
                }

            }
        });

        }

        kafkaProducer.flush();
        kafkaProducer.close();
    }
}
