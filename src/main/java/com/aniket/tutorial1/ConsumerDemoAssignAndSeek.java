package com.aniket.tutorial1;

import com.google.gson.Gson;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ConsumerDemoAssignAndSeek {
    public static final Logger logger = LoggerFactory.getLogger(ConsumerDemoAssignAndSeek.class.getName());

    public static void main(String[] args) {

        //Set Consumer properties
        Properties properties = new Properties();
        //Create the Consumer
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"127.0.0.1:9092");
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
       // properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "my_group");
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");



        KafkaConsumer<String,String> kafkaConsumer = new KafkaConsumer(properties);

        // kafkaConsumer.subscribe(Arrays.asList("first_topic"));

        //Assign can be used to attach Consumer to a specific Topic specific partition
        TopicPartition topicPartition = new TopicPartition("first_topic",0);
        kafkaConsumer.assign(Arrays.asList(topicPartition));

        //Seek can be used to read from a specific offset of a specific partition
        //of a specific topic consumer is assigned to
        kafkaConsumer.seek(topicPartition,25L);

        int msgProcessed = 5;
        boolean keeponReading = true;

        //Lets process only 5 messages then exit
         while (keeponReading)
         {

            ConsumerRecords<String,String> records =   kafkaConsumer.poll(Duration.ofMillis(1000));
             for (ConsumerRecord record:records)
             {
                 logger.info("Message Info for "+(5-msgProcessed)+" : "+new Gson().toJson(record));
                 msgProcessed--;
                 if(msgProcessed==0)
                 {
                     keeponReading=false;
                     break;
                 }
             }
         }

        logger.info("*******GOOD BYEEE************");
    }
}
